import subprocess
import copy
import os
import sys
import Base


class hypre(Base.base):
  def __init__(self,loggers=[sys.stdout]):
    Base.base.__init__(self,loggers)
    self.configtype='configure'
    version="2015.05.07"
    self.downloadfile='http://ftp.mcs.anl.gov/pub/petsc/externalpackages/hypre-'+version+'.tar.gz'
    

    self.packagename="Hypre"
    self.dirname="hypre-"+version
    self.builddirname="hypre-"+version
    self.zipfile="hypre-"+version+".tar.gz"
    self.extractor="tar"
    self.exflags="xzf"
    self.configoptions=[]
    self.configlog=os.path.join('src','config.log')
    self.configkeys=['CC=','CFLAGS=','CPP=','CPPFLAGS=','CXX=','CXXFLAGS=','F77=','F77FLAGS=','FFLAGS=','LDFLAGS=','BLASLIBS=','LAPACKLIBS=']



  def mysetup(self):
    configure = copy.deepcopy(self.options.configopts())
    os.chdir('src')
    print configure
    configure.insert(0,'./configure')
    self.logitem(' '.join(configure)+"\n")
    output = self.system(configure)
    self.logitem(output)
    self.logitem("Hypre return code: %d\n" % self.status)
    

  
  def mybuild(self):
    os.chdir('src')
    output = self.system(['make'])
    self.logitem(output)


  def mytest(self):
    os.chdir('src')
    output = self.system(['make','test'])
    self.logitem(output)

