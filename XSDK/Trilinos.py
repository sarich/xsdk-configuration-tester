import subprocess
import copy
import os
import sys
import shutil
import Base

class trilinos(Base.base):
  def __init__(self,loggers=[sys.stdout]):
    Base.base.__init__(self,loggers)
    self.configtype='cmake'
    self.giturl="https://github.com/trilinos/trilinos"
    #self.downloadfile='http://trilinos.csbsju.edu/download/files/trilinos-11.14.3-Source.tar.gz'
    #self.zipfile="trilinos-11.14.3-Source.tar.gz"
    #self.extractor="tar"
    #self.exflags="xzf"
    self.packagename="Trilinos"
    self.dirname="trilinos"
    self.builddirname="trilinos-Build"
    self.configlog="CMakeCache.txt"
    self.configkeys=['CMAKE_CXX_COMPILER:FILEPATH=',
                     'CMAKE_CXX_FLAGS:STRING=',
                     'CMAKE_CXX_FLAGS_DEBUG:STRING=',
                     'CMAKE_C_COMPILER:FILEPATH=',
                     'CMAKE_C_FLAGS:STRING=',
                     'CMAKE_C_FLAGS_DEBUG:STRING=',
                     'CMAKE_Fortran_COMPILER:FILEPATH=',
                     'CMAKE_Fortran__FLAGS:STRING=',
                     'CMAKE_Fortran__FLAGS_DEBUG:STRING=',
                     'CMAKE_BUILD_TYPE:STRING=',
                     'BUILD_SHARED_LIBS:BOOL=',
                     'TPL_ENABLE_LAPACK:STRING=',
                     'TPL_ENABLE_MPI:BOOL=',
                     'Trilinos_ENABLE_C:BOOL=',
                     'Trilinos_ENABLE_CXX:BOOL=',
                     'Trilinos_ENABLE_DEBUG:BOOL='
                     'Trilinos_ENABLE_Fortran:BOOL=',
                     'Trilinos_EXTRA_LINK_FLAGS:STRING=',
                     'XSDK_ENABLE_CXX:',
                     'XSDK_ENABLE_DEBUG:',
                     'XSDK_ENABLE_FORTRAN:',
                     'XSDK_INDEX_SIZE:',
                     'XSDK_PRECISION:',
                     'TPL_BLAS_LIBRARIES:',
                     'TPL_LAPACK_LIBRARIES:',
                     'USE_XSDK_DEFAULTS:'                  ]

                     

  def mysetup(self):
      try:
        os.remove('CMakeCache.txt')
      except:
        pass
      doconfigfile = open('do-configure','w')
      doconfigfile.write("#!/bin/sh\n\n")
      doconfigfile.write("EXTRA_ARGS=$@\n\n")
      doconfigfile.write("cmake \\\n")
      doconfigfile.write('-D Trilinos_ASSERT_MISSING_PACKAGES=OFF \\\n')
      doconfigfile.write('-D USE_XSDK_DEFAULTS=YES \\\n')
      doconfigfile.write('-D TPL_ENABLE_MPI=ON \\\n')
      doconfigfile.write('-D Trilinos_ENABLE_CTrilinos=ON \\\n')
      doconfigfile.write(' \\\n'.join(self.options.cmakeopts()))
      doconfigfile.write(" \\\n$EXTRA_ARGS \\\n")
      doconfigfile.write("../%s\n" % (self.dirname))
      doconfigfile.close()
      doconfigfile = open('do-configure','r')
      for line in doconfigfile.readlines():
        self.logitem(line)
      doconfigfile.close()
      os.chmod('do-configure',0764)
      cl = ['./do-configure']
      print self.commandline()
      cl.extend(self.commandline())
      self.logitem('Command Line: %s\n' % ' '.join(cl))
      output = self.system(cl)
      self.logitem(output)
      if not os.path.isfile('CMakeCache.txt'):
        self.status=-1
      self.logitem("do-configure return code: %d\n" % self.status )

  def cleanup(self):
    shutil.rmtree(os.path.join(self.packagedir,self.builddirname),ignore_errors=True)
                     
  def mybuild(self):
    pass
      
