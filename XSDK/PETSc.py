import subprocess
import copy
import os
import sys
import Base


class petsc(Base.base):
  def __init__(self,loggers=[sys.stdout]):
    Base.base.__init__(self,loggers)
    self.configtype='configure'
    self.giturl='https://petsc@bitbucket.org/petsc/petsc'
    self.gitbranch='master'
    self.packagename="PETSc"
    self.dirname="petsc"
    self.builddirname="petsc"
    self.configlog="configure.log"
    self.configkeys=['  C Compiler:',  
                     '  C++ Compiler:',
                     '  Fortran Compiler:',
                     'BLAS/LAPACK:',
                     '  Integer size:',
                     '  shared libraries:',
                     '  Scalar type:',
                     '  Precision:']
                     
                     
    self.configoptions=['--with-xsdk-defaults']
  def mysetup(self):
    configure = copy.deepcopy(self.options.configopts())
    configure.insert(0,'./configure')
    configure.extend(self.configoptions)

    self.logitem(' '.join(configure)+"\n")
    output = self.system(configure)
    self.logitem(output)
    self.logitem("PETSc return code: %d\n" % self.status)
    

  
  def mybuild(self):
    output = self.system(['make'])
    self.logitem(output)

  def mytest(self):
    output = self.system(['make','test'])
    self.logitem(output)

