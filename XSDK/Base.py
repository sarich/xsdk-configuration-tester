#!/usr/bin/env python
import os
import sys
import subprocess
import re
import shutil
import socket

class reportItem:
  def __init__(self,packagename,testname,key,searchline,success,check='FOR'):
    self.package=packagename
    self.testname=testname
    self.key=key
    self.searchline=searchline.rstrip('\n')
    self.success=success
    self.check=check.upper()


  def __str__(self):
    if self.success:
      if self.check=='FAILURE':
        retval = "configure failed as it should"
      else:
        retval="Checking %s value '%s' in '%s': SUCCESS\n" % (self.check.lower(),self.key,self.searchline)
    else:
      retval="FAILED TEST \n'%s' Failed in package %s\n" % (self.testname,self.package)
      retval+="Was checking %s value '%s' in %s\n" % (self.check.lower(),self.key,self.searchline)
        
    return retval



class base:
  def __init__(self,loggers=[sys.stdout]):
    self.hostname=socket.gethostname()
    if self.hostname.find('stokes')>=0:
      self.packagedir='/sandbox/petsc/xsdkpackages'
    else:
      self.packagedir='packages'
    self.packagename = ""
    self.logs=loggers
    self.downloadfile = None
    self.extractor = None
    self.exflags = None
    self.giturl = None
    self.gitbranch = None
    self.dirname = None
    self.builddirname = None
    self.options = None
    self.configtype=''
    self.configlog = None
    self.configkeys = []
    self.textreport={}
    self.status = 0


  def download(self):
    if not os.path.exists(self.packagedir):
      os.mkdir(self.packagedir)
    cwd = os.getcwd()
    os.chdir(self.packagedir)

    if self.downloadfile is not None:
      if os.path.exists(self.zipfile):
        sys.stdout.write('%s exists, skipping download\n' % self.zipfile)
      else:
        subprocess.call(['curl','-o',self.downloadfile[self.downloadfile.rfind('/')+1:],self.downloadfile])
      
      subprocess.call([self.extractor,self.exflags,self.zipfile])
      
    elif self.giturl is not None:
      if os.path.exists(self.dirname):
        sys.stdout.write('%s git repo exists, pulling...\n' % self.dirname)
        os.chdir(self.dirname)
        subprocess.call(['git','pull'])
      else:
        sys.stdout.write('cloning %s into packages/%s\n' % (self.giturl, self.dirname))
        subprocess.call(['git','clone',self.giturl])
      if self.gitbranch is not None and self.gitbranch != "":
        subprocess.call(['git','checkout',self.gitbranch])
    else:
      sys.stderr.write('Error: Download url for %s undefined\n' % self.packagename)
      sys.exit(1)
    os.chdir(cwd)

  def logitem(self,s):
    for l in self.logs:
      l.write(s)

  def system(self,commandlist):
    try:
      output = subprocess.check_output(commandlist)
      self.status = 0
    except subprocess.CalledProcessError, ex:
      self.status = ex.returncode
      output = ex.output
    return output

  def setup(self):
    self.status=0
    if hasattr(self,'cleanup'):
      self.cleanup()
    try:
      os.remove(self.configlog)
    except:
      pass
    cwd = os.getcwd()
    builddir = os.path.join(self.packagedir,self.builddirname)
    if not os.path.exists(builddir):
      os.mkdir(builddir)

    os.chdir(builddir)
    self.options.updateenv()
    self.mysetup()
    os.chdir(cwd)
    if self.status != 0:
      sys.stderr.write("Failed to configure\n")
      self.loglines=[]

    
    # Get lines from log file we've considered to be relevant
    self.loglines=[]
    f = open(os.path.join(builddir,self.configlog),'r')
    lines = f.readlines()
    for l in lines:
      for o in self.configkeys:
        m = re.match(o.replace('+','\+')+'.*$',l)
        if m:
          self.loglines.append(l.rstrip('\n'))

  def build(self):
    cwd = os.getcwd()
    os.chdir(os.path.join(self.packagedir,self.builddirname))
    self.mybuild()
    os.chdir(cwd)

  def test(self):
    cwd = os.getcwd()
    os.chdir(self.dirname)
    self.mytest()
    os.chdir(cwd)

  def setOptions(self,newoptions):
    self.options = newoptions
    #for l in self.logs:
    #  l.write(str(self.options))

  def checkOptions(self):
    if self.options is not None:
      cwd = os.getcwd()
      os.chdir(self.builddirname)
      for k in self.options.EVARS.keys():
        pass

  def banner(self):
    return("\n\n***************************************\n" +
           "*************** %s ********************" % self.packagename[0:39]+
           "***************************************\n\n")

  def checkSetup(self):
    reports = []
    if self.options.shouldfail and  self.status != 0:
      reports.append(reportItem(self.packagename,self.options.name,"","",True,check='FAILURE'))
      return reports
      
      
    if self.options.checkfor.has_key(self.packagename):
      checkforlist = self.options.checkfor[self.packagename]
      for k in checkforlist:
        found = False
        for l in self.loglines:
          if not found and l.find(k)>=0:
            status = (l[len(k):].find(checkforlist[k])>=0)
            found = True
            reports.append(reportItem(self.packagename,self.options.name,checkforlist[k],l,status,'FOR'))
        if not found:
          reports.append(reportItem(self.packagename,self.options.name,checkforlist[k],k,False,'FOR'))
          



    if self.options.checkagainst.has_key(self.packagename):
      checkagainstlist = self.options.checkagainst[self.packagename]
      for k in checkagainstlist:
        found = False
        for l in self.loglines:
          if l.startswith(k):
            if not found:
              found = True
              status = (l.find(checkagainstlist[k])<0)
              reports.append(reportItem(self.packagename,self.options.name,checkagainstlist[k],l,status,'AGAINST'))
        if not found:
          print self.packagename
          print self.options.name
          print k
          print checkagainstlist
          print checkagainstlist[k]

          reports.append(reportItem(self.packagename,self.options.name,checkagainstlist[k],"LINE NOT FOUND",False,'AGAINST'))
    return reports

  def commandline(self):
    return self.options.cmakecommandlineopts()
    
  def printOptionsDesired(self,fd):
    if len(self.options.evars)==0:
      fd.write("Environment variables: None found\n")
    else:
      fd.write("Environment variables:\n")
      for k in self.options.evars:
        fd.write("%s=%s\n" % (k,self.options.evars[k]))
    if len(self.options.cvars)==0:
      fd.write("Command line variables: None found\n")
    else:
      fd.write("Command line variables:\n")
      for k in self.options.cvars:
        fd.write("%s=%s\n" % (k,self.options.cvars[k]))
        
    opts=self.options.getOptions(self.configtype)
    if len(opts) ==0:
      fd.write("No Options Selected -- using defaults\n")
    else:
      fd.write("Options requested: "+" ".join(opts)+"\n")

    
  def printOptionsFound(self,fd):
    fd.write("\n\nOptions detected after configure/cmake:\n")
    f = open(os.path.join(self.packagedir,self.builddirname,self.configlog),'r')
    lines = f.readlines()
    for o in self.loglines:
      fd.write(o+'\n')


    if len(self.options.fail) > 0:
      fd.write("Failed test %s because:\n" % self.options.name)
      for f in self.options.fail:
        fd.write("\t%s\n" % (self.report[f]))

               
