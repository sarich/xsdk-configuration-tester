import os
import socket

class defaultoptions:
  ENVVARS=['CC','CXX','FC','CPP','CFLAGS','FFLAGS','FCFLAGS','CXXFLAGS','CPPFLAGS','LDFLAGS']
  CMAKE_EVAR_MAP={'CC':'CMAKE_C_COMPILER',
                  'CFLAGS':'CMAKE_C_FLAGS',

                  'CXXFLAGS':'CMAKE_CXX_FLAGS',
                  'CXX':'CMAKE_CXX_COMPILER',

                  'FC':'CMAKE_Fortran_COMPILER',
                  'FCFLAGS':'CMAKE_Fortran_FLAGS'}


  def __init__(self,name="XSDK defaults"):
    # ignore existing environment variables
    self.name = name
    self.description="Tests default XSDK options"
    self.checkfor={}
    self.shouldfail=False
    self.fail=[]
    self.checkagainst={}
    # These will be set as environmental variables  
    self.evars={}
    # These will be set as internal variables on the configure command line.
    # According to the specs, these values should supercede anything set in
    # the environmnent variables.
    self.cvars={}

    self.updateenv()
    self.options={'debug':True,'shared':True,'fortran':True,'c++':True,'precision':'double','index-size':32}
    

    self.templateopts={}
    self.badlocalopts={'hdf5libflags':"-L/usr/lib/x86_64-linux-gnu -lhdf5_fortran -lhdf5_hl -lhdf5",
                    'hdf5includeflags':"",
                    'blaslibflags':"-lblas",
                    'lapacklibflags':"-llapack"}

  

  def configopts(self):
    retval=[]
    for k in self.cvars:
      retval.append('%s=%s' % (k,self.cvars[k]))
    if self.options.has_key('debug'):
      if self.options['debug']:
        retval.append('--enable-debug')
      else:
        retval.append('--disable-debug')
    else:
      retval.append('--enable-debug')

    if self.options.has_key('shared'):
      if self.options['shared']:
        retval.append('--enable-shared')
      else:
        retval.append('--disable-shared')
    else:
      retval.append('--enable-shared')

    if self.options.has_key('fortran'):
      if self.options['fortran']:
        retval.append('--enable-fortran')
      else:
        retval.append('--disable-fortran')
    else:
      retval.append('--enable-fortran')

    if self.options.has_key('c++'):
      if self.options['c++']:
        retval.append('--enable-cxx')
      else:
        retval.append('--disable-cxx')
    else:
      retval.append('--enable-cxx')

    if self.options.has_key('precision'):
      retval.append('--with-precision='+self.options['precision'])
    else:
      retval.append('--with-precision=double')

    if self.options.has_key('index-size'):
      retval.append('--with-index-size='+str(self.options['index-size']))
    else:
      retval.append('--with-index-size=32')

    if self.options.has_key('blas'):
      retval.append('--with-blas-lib=' + str(self.options['blas']))

    if self.options.has_key('lapack'):
      retval.append('--with-lapack-lib=' + str(self.options['lapack']))

    return retval

  def cmakecommandlineopts(self):
    cl = []
    for k in self.cvars:
      v = self.cvars[k]
      if self.CMAKE_EVAR_MAP.has_key(k):
        cl.append('-D %s=%s' %(self.CMAKE_EVAR_MAP[k],v))
    return cl

  def cmakeopts(self):
    retval=[]
    if self.options.has_key('debug'):
      if self.options['debug']:
        retval.append('-D CMAKE_BUILD_TYPE=DEBUG')
        retval.append('-D XSDK_ENABLE_DEBUG=YES')
      else:
        retval.append('-D CMAKE_BUILD_TYPE=RELEASE')
        retval.append('-D XSDK_ENABLE_DEBUG=NO')
    else:
      retval.append('-D CMAKE_BUILD_TYPE=DEBUG')
      retval.append('-D XSDK_ENABLE_DEBUG=YES')


    if self.options.has_key('shared'):
      if self.options['shared']:
        retval.append('-D BUILD_SHARED_LIBS=YES')
      else:
        retval.append('-D BUILD_SHARED_LIBS=NO')
    else:
      retval.append('-D BUILD_SHARED_LIBS=YES')

    if self.options.has_key('fortran'):
      if self.options['fortran']:
        retval.append('-D XSDK_ENABLE_FORTRAN=YES')
      else:
        retval.append('-D XSDK_ENABLE_FORTRAN=NO')
    else:
      retval.append('-D XSDK_ENABLE_FORTRAN=YES')

    if self.options.has_key('c++'):
      if self.options['c++']:
        retval.append('-D XSDK_ENABLE_CXX=YES')
      else:
        retval.append('-D XSDK_ENABLE_CXX=NO')
    else:
      retval.append('-D XSDK_ENABLE_CXX=YES')

    if self.options.has_key('precision'):
      retval.append('-D XSDK_PRECISION='+self.options['precision'].upper())
    else:
      retval.append('-D XSDK_PRECISION=DOUBLE')

    if self.options.has_key('index-size'):
      retval.append('-D XSDK_INDEX_SIZE='+str(self.options['index-size']))
    else:
      retval.append('-D XSDK_INDEX_SIZE=32')

    if self.options.has_key('blas'):
      retval.append('-D TPL_BLAS_LIBRARIES="' + str(self.options['blas'])+'"')

    if self.options.has_key('lapack'):
      retval.append('-D TPL_LAPACK_LIBRARIES="' + str(self.options['lapack'])+'"')
      
    return retval
    
  
  def __str__(self):
    estr = ""
    for k in self.evars.keys():
        if (os.environ.has_key(k)):
            estr += "%s: %s\n" % (k,os.environ[k])
    return "environment:%s\nConfig:%s\n%s\nCMAKE:%s\n" % (estr,self.evars,self.configopts(),self.cmakeopts())

  def getOptions(self,mytype):
    if mytype.find('configure')==0:
      return self.configopts()
    else:
      return self.cmakeopts()

      


  def updateenv(self):
    for k in self.ENVVARS:
      if k in os.environ:
        os.environ.pop(k)
    for k in self.evars.keys():
        if self.evars[k] != '':
            os.environ[k] = self.evars[k]


class TestList:
  """
default values:
evars: 'CC','CXX','FC','CPP','CFLAGS','FFLAGS','FCFLAGS','CXXFLAGS','CPPFLAGS','LDFLAGS'
cvars: 'CC','CXX','FC','CPP','CFLAGS','FFLAGS','FCFLAGS','CXXFLAGS','CPPFLAGS','LDFLAGS'
options:    'debug':True,
            'shared':True,
            'fortran':True,
            'c++':False,
            'precision':'double',
            'index-size':32
  """

  def __init__(self):
    cwd = os.getcwd()
    hostname=socket.gethostname()
    if hostname.find('stokes')>=0:
      self.packagedir='/sandbox/petsc/xsdkpackages'
    else:
      self.packagedir=os.path.join(cwd,'packages')
    self.tests=[]
    test = defaultoptions()
    test.checkfor['Hypre']={'CFLAGS=':''}
    test.checkfor['PETSc']={'  C Compiler':'-g',
                            '  C++ Compiler':'-g',
                            '  Fortran Compiler':'-g',
                            '  shared libraries':'enabled',
                            '  Scalar type':'real',
                            '  Integer size':'32',
                            '  Precision':'double'}
               
    test.checkfor['Trilinos']={'XSDK_ENABLE_DEBUG':'YES',
                               'BUILD_SHARED_LIBS':'YES',
                               'XSDK_PRECISION':'DOUBLE',
                               'XSDK_INDEX_SIZE':'32'
                             }
    
    self.tests.append(test)
    


    test = defaultoptions("check given blaslapack")
    test.options['blas']=os.path.join(cwd,self.packagedir,'xsdkblaslapack','libf2cblas.a')
    test.options['lapack']=os.path.join(cwd,self.packagedir,'xsdkblaslapack','libf2clapack.a')
    test.checkfor['PETSc'] = {'BLAS/LAPACK':'xsdkblaslapack'}
    test.checkfor['Trilinos'] = {'TPL_BLAS_LIBRARIES':'xsdkblaslapack',
                                 'TPL_LAPACK_LIBRARIES':'xsdkblaslapack'}
    test.checkfor['Hypre'] = {'BLASLIBS':'xsdkblaslapack',
                              'LAPACKLIBS':'xsdkblaslapack'}
    test.description="Get blas/lapack from command line options"
    self.tests.append(test)


    test = defaultoptions("check bad c compiler")
    test.evars['CC']='nosuchcompiler'
    test.checkfor['Hypre']={'CC=':'nosuchcompiler'}
    test.checkfor['PETSc']={'  C Compiler':'nosuchcompiler'}
    test.checkfor['Trilinos']={'CMAKE_C_COMPILER':'nosuchcompiler'}
    test.description="Gives bad C compiler to environment variable CC, expected to fail"
    test.shouldfail = True
    
    self.tests.append(test)



    test = defaultoptions("check no debugging")
    test.options['debug']=False
    test.checkfor['PETSc']={'  C Compiler':'-O',
                            '  C++ Compiler':'-O',
                            '  Fortran Compiler':'-O'}
    test.checkagainst['Trilinos']={'XSDK_ENABLE_DEBUG':'YES'}
    test.description="Turn off debugging, expects -O option"
    self.tests.append(test)


    test = defaultoptions("check command line overrides")
    test.evars['CC']='bad c compiler'
    test.evars['CXX']='bad cxx compiler'
    test.evars['FC']='bad fortran compiler'
    test.cvars['CC']='mpicc'
    test.cvars['CXX']='mpicxx'
    test.cvars['FC']='mpif90'
    test.checkfor['PETSc'] = {'  C Compiler':'mpicc',
                              '  C++ Compiler':'mpicxx',
                              '  Fortran Compiler':'mpif90'}
    test.checkfor['Hypre'] = {'CC=':'mpicc',
                              'CXX=':'mpicxx',
                              'F77=':'mpif90'}
    test.checkfor['Trilinos']={'CMAKE_C_COMPILER':'mpicc',
                               'CMAKE_CXX_COMPILER':'mpicxx',
                               'CMAKE_Fortran_COMPILER':'mpif90'}
    test.description="Gives one compiler to command line, another to environment variables. Expecting the command line environment variable to override the environment variable"
    self.tests.append(test)

    test = defaultoptions("check no shared")
    test.options['shared']=False
    test.checkfor['PETSc'] = {'  shared libraries':'disabled'}
    test.checkfor['Trilinos']={'BUILD_SHARED_LIBS':'NO'}
    test.description="Check that shared libraries are configured to build"
    self.tests.append(test)

    test = defaultoptions("check single precision")
    test.options['precision']='single'
    test.checkfor['PETSc']={'  Precision':'single'}
    test.checkfor['Trilinos']={'XSDK_PRECISION':'SINGLE'}
    test.description="Check that single precision is configured (if applicable)"
    self.tests.append(test)
    
    test = defaultoptions("check quad precision")
    test.options['precision']='quad'
    test.options['blas']=os.path.join(cwd,self.packagedir,'xsdkblaslapack-quad','libf2cblas.a')
    test.options['lapack']=os.path.join(cwd,self.packagedir,'xsdkblaslapack-quad','libf2clapack.a')
    test.checkfor['PETSc']={'  Precision':'__float128'}
    test.checkfor['Trilinos']={'XSDK_PRECISION':'QUAD'}
    test.description="Check that quad precision is configured (if applicable)"
    self.tests.append(test)

    test = defaultoptions("check index 64")
    test.options['index-size']=64
    test.checkfor['PETSc']={'  Integer size':'64'}
    test.checkfor['Trilinos']={'XSDK_INDEX_SIZE':'64'}
    test.description="Check that 64-bit indices are configured (if applicable)"
    self.tests.append(test)
                          


    
  
    
