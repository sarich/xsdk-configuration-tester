#!/usr/bin/env python
import os
import sys
import datetime
import XSDK
import XSDK.PETSc
import XSDK.Hypre
import XSDK.Trilinos
import XSDK.Tests
failures = []
log = file('xsdk-test.log','w')
logs=[log,sys.stdout]
hypre = XSDK.Hypre.hypre(loggers=logs)
petsc = XSDK.PETSc.petsc(loggers=logs)
trilinos = XSDK.Trilinos.trilinos(loggers=logs)
tests = XSDK.Tests.TestList().tests
packages = [trilinos,hypre,petsc]
if os.environ.has_key('PETSC_DIR'):
  os.environ.pop('PETSC_DIR')
if os.environ.has_key('PETSC_ARCH'):
  os.environ.pop('PETSC_ARCH')

now = str(datetime.datetime.now())
for l in logs:
  l.write(now)
  l.write("\n-------------------------------------------------------\n")



for package in packages:
  for l in [log,sys.stdout]:
    l.write(package.banner())

  package.download()
  for test in tests:
    singlefilelog = file("%s-%s.txt" % (package.packagename,test.name.replace(' ','-')),"w")
    logs.append(singlefilelog)
    if not hasattr(test,'status'):
        test.status={}

    if not (test.checkfor.has_key(package.packagename) or test.checkagainst.has_key(package.packagename)):
      continue
      
    singlefilelog.write(str(datetime.datetime.now())+"\n")
    for l in logs:
      l.write("\n----------------------------------------\n")
      l.write("\nTesting: " + package.packagename + " " + test.name + ": \n")
      l.flush()
    singlefilelog.write("\n"+test.description+"\n\n")
      

    package.setOptions(test)
    for l in logs:
      package.printOptionsDesired(l)
    package.setup()
    results = package.checkSetup()
    success=True
    for l in logs:
      package.printOptionsFound(l)
    for r in results:
      for l in logs:
        l.write(str(r))
        if not r.success:
            success=False
            failures.append(r)
            sys.stdout.write(str(r))
            package.printOptionsDesired(sys.stdout)
    if success:
      for l in logs:
        l.write("PASSED\n")
        test.status[package.packagename]="PASSED"
    else:
      for l in logs:
        l.write("FAILED " + package.packagename + "  " + test.name+"\n")
        test.status[package.packagename]="FAILED"
    logs.remove(singlefilelog)

# write html page
index = open("index.html","w")
index.write('<html><head><title>XSDK Test results</title></head><body>\n')
index.write(now+'<br>\n')
index.write('<table>\n')
index.write('<thead>\n  <th></th>\n')
for p in packages:
    index.write('  <th>' + p.packagename + '</th>\n')
index.write('</thead><tbody>\n')
for test in tests:
  index.write('<tr>\n  <th>'+test.name+'</th>\n')
  for p in packages:
    if test.checkfor.has_key(p.packagename) or test.checkagainst.has_key(p.packagename):
      index.write('  <td><a href="%s-%s.txt">%s</a></td>\n' % (p.packagename,test.name.replace(' ','-'),test.status[p.packagename]))
    else:
      index.write('  <td></td>\n')
  index.write('</tr>\n')
index.write('Source code for tester can be bound on <a href="https://bitbucket.org/sarich/xsdk-configuration-tester">bitbucket</a>\n')
index.write('</tbody></table></body></html>\n')
              
        

if len(failures):
    print "----------------------------------------\n"
    print "At least one test failed:"
    for f in failures:
      for l in logs:
        l.write(str(f))

for l in logs:
  l.write(now)
  l.write( "\n-------------------------------------------------------\n")
  l.write("\n                               |")
  for p in packages:
    l.write("%10.10s |" % p.packagename)
  l.write("\n")


  for test in tests:
    l.write("%-30.30s |" % test.name)
    for p in packages:
      if test.checkfor.has_key(p.packagename) or test.checkagainst.has_key(p.packagename):
        l.write("%10.10s |" % (test.status[p.packagename]))
      else:
        l.write("           |")
        
    l.write("\n")
    
  l.write('\n\nCheck file xsdk-test.log for detailed information\n')
log.close()

